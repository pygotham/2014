=============
PyGotham 2014
=============

The PyGotham 2014 website. This archived is released under the same license as
[PyGotham](https://github.com/PyGotham/pygotham).

This can be recreated with::

    wget --recursive --page-requisites --convert-links --no-parent --html-extension 2014.pygotham.org/
